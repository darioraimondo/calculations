package com.example.minimal.controller;

import com.example.minimal.dto.ArrayRequest;
import com.example.minimal.service.MainService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class MainController {

    private MainService mainService;

    @Autowired
    MainController(MainService mainService){
        this.mainService = mainService;
    }

    @GetMapping("/")
    public ResponseEntity<?> getHomePage() {
        return ResponseEntity.ok().body("Welcome to the minimal");
    }

    @PostMapping("/arrays")
    public ResponseEntity<?> getMinimumInteger(@RequestBody ArrayRequest numbers) {

        log.info("showing numbers: "+ numbers);
        return ResponseEntity.ok().body(mainService.getMinimumInteger(numbers.getNumbers()));
    }


}
