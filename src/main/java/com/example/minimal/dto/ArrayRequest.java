package com.example.minimal.dto;

import lombok.*;

import java.util.ArrayList;

@Setter
@Getter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class ArrayRequest {
    private int[] numbers;
}
